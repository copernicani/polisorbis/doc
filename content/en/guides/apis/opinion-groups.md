---
title: Math/PCA API
date: 2024-11-06
description: opunion groups and PCA analysis (advanced).
weight: 2
---
This API is used to get all grouping information produced by the PCA algorithm. This route gives the information contained in the reports All Statement table, specifically for the groups columns, as well as all other information in the report, although not in a machine readable format.

This page contains an example of a request and a response given from the API, with a schema describing the significant fields that can be found in the response (we will describe just the "group-votes").

## Example: Survey on ice cream

### Request example
```
GET http://<polisorbis-server-domain>/api/v3/math/pca2?lastVoteTimestamp=0&conversation_id=<survey-id>
```

### Response example
This JSON is partial: an actual response contains other fields that are not specified here. The following are the more commonly used properties.

```
{
  "n": 10,
  "tids": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
  "mod-in": [7, 1, 4, 3, 2],
  "n-cmts": 11,
  "in-conv": [7, 1, 4, 6, 3, 2, 9, 5, 10, 8],
  "mod-out": [],
  "group-votes": {
    "0": {
      "votes": {
        "0": {
          "A": 5,
          "D": 0,
          "S": 5
        },
        "1": {
          "A": 3,
          "D": 0,
          "S": 5
        },
        "2": {
          "A": 4,
          "D": 0,
          "S": 5
        },
        "3": {
          "A": 0,
          "D": 5,
          "S": 5
        },
        "4": {
          "A": 3,
          "D": 2,
          "S": 5
        },
        "5": {
          "A": 4,
          "D": 2,
          "S": 6
        },
        "6": {
          "A": 5,
          "D": 0,
          "S": 5
        },
        "7": {
          "A": 5,
          "D": 0,
          "S": 5
        },
        "8": {
          "A": 3,
          "D": 0,
          "S": 5
        },
        "9": {
          "A": 5,
          "D": 0,
          "S": 6
        },
        "10": {
          "A": 4,
          "D": 0,
          "S": 5
        }
      },
      "n-members": 7,
      "id": 0
    },
    "1": {
      "votes": {
        "0": {
          "A": 1,
          "D": 0,
          "S": 2
        },
        "1": {
          "A": 0,
          "D": 2,
          "S": 2
        },
        "2": {
          "A": 0,
          "D": 0,
          "S": 3
        },
        "3": {
          "A": 2,
          "D": 1,
          "S": 3
        },
        "4": {
          "A": 2,
          "D": 1,
          "S": 3
        },
        "5": {
          "A": 1,
          "D": 1,
          "S": 3
        },
        "6": {
          "A": 0,
          "D": 2,
          "S": 3
        },
        "7": {
          "A": 1,
          "D": 2,
          "S": 3
        },
        "8": {
          "A": 0,
          "D": 2,
          "S": 3
        },
        "9": {
          "A": 1,
          "D": 2,
          "S": 3
        },
        "10": {
          "A": 1,
          "D": 2,
          "S": 3
        }
      },
      "n-members": 3,
      "id": 1
    }
  },
  "group-clusters": [
    {
      "id": 0,
      "center": [-1.0729096569696, 0.0648828125178037],
      "members": [3, 5, 6, 8, 9, 11]
    },
    {
      "id": 1,
      "center": [2.27779024648904, -0.4714273062786],
      "members": [0, 10, 12]
    }
  ],
  "lastVoteTimestamp": 1730728069749,
  "comment-priorities": {
    "0": 7.18329418948676,
    "1": 5.02356276961004,
    "2": 1.15593698880458,
    "3": 17.2447553781763,
    "4": 22.6101843262524,
    "5": 12.6261367613404,
    "6": 12.013416432267,
    "7": 15.8755313882589,
    "8": 3.30400426158883,
    "9": 9.97027674129075,
    "10": 10.889094606946
  }
}
```

The following JSON schema provides a description of the fields listed above
```
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "n": {
      "type": "integer",
      "description": "Number of users grouped by PCA (is not necessarily equal to participant count)"
    },
    "tids": {
      "type": "array",
      "items": { "type": "integer" },
      "description": "Array of comment IDs in the survey"
    },
    "mod-in": {
      "type": "array",
      "items": { "type": "integer" },
      "description": "Comment IDs accepted by moderators"
    },
    "n-cmts": {
      "type": "integer",
      "description": "Number of comments in the survey"
    },
    "in-conv": {
      "type": "array",
      "items": { "type": "integer" },
      "description": "Array of user IDs who participated in the survey"
    },
    "mod-out": {
      "type": "array",
      "items": { "type": "integer" },
      "description": "Comment IDs rejected by moderators"
    },
    "group-votes": {
      "type": "object",
      "additionalProperties": {
        "type": "object",
        "properties": {
          "votes": {
            "type": "object",
            "additionalProperties": {
              "type": "object",
              "properties": {
                "A": {
                  "type": "integer",
                  "description": "Agree votes count"
                },
                "D": {
                  "type": "integer",
                  "description": "Disagree votes count"
                },
                "S": {
                  "type": "integer",
                  "description": "Sum count (agree + disagree + pass)"
                }
              },
              "required": ["A", "D", "S"]
            },
            "description": "Vote breakdown per topic per group"
          },
          "n-members": {
            "type": "integer",
            "description": "Number of members in the group",
            "example": 7
          },
          "id": {
            "type": "integer",
            "description": "Unique identifier for the group",
            "example": 0
          }
        },
        "required": ["votes", "n-members", "id"]
      },
      "description": "Votes breakdown by group"
    },
    "group-clusters": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "type": "integer",
            "description": "Cluster ID"
          },
          "center": {
            "type": "array",
            "items": { "type": "number" },
            "description": "Coordinates of the cluster center in PCA space"
          },
          "members": {
            "type": "array",
            "items": { "type": "integer" },
            "description": "List of user IDs in the cluster"
          }
        },
        "required": ["id", "center", "members"]
      },
      "description": "Array of clusters with PCA centers and member IDs"
    },
    "lastVoteTimestamp": {
      "type": "string",
      "description": "Timestamp of the last vote (epoch format)",
      "example": "1730728069749"
    },
    "comment-priorities": {
      "type": "object",
      "additionalProperties": {
        "type": "number",
        "description": "Priority score for each comment"
      },
      "description": "Mapping of comment IDs to priority scores: the highest priority comments are visualized first"
    }
  },
  "required": [
    "n",
    "tids",
    "mod-in",
    "n-cmts",
    "in-conv",
    "mod-out",
    "group-votes",
    "group-clusters",
    "lastVoteTimestamp",
    "comment-priorities"
  ]
}
```