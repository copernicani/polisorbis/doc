---
title: APIs
date: 2024-11-06
description: Access to Survey data from machines
weight: 5
---
A survey's machine readable information can be obtained through the PolisOrbis component "server" API using the APIs that returns the same  data that build up the report (see [example here](example-report.pdf)) but as a json object.
