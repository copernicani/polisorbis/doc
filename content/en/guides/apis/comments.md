---
title: Comments API
date: 2024-11-06
description: get all survey's comments and votes.
weight: 1
---
This API is used to get all survey's comments and votes. This route gives the information contained in the report's All Statement table, specifically for the STATEMENT and OVERALL columns.

![statements-overall](../statements-overall.png)


This page contains an example of a request and a response given from the API, with a schema describing the significant fields that can be found in the response.

## Example: Survey on ice cream

### Request example
```
GET http://<polisorbis-server-domain>/api/v3/comments?moderation=true&mod_gt=-1&include_voting_patterns=true&conversation_id=<survey-id>
```

### Response example
This JSON is partial: an actual response contains other fields that are not specified here. The following are the more commonly used properties.

```
[
  {
    "txt": "Mint chocolate chip is the perfect blend of refreshing mint and rich chocolate, making every scoop a delightful experience!",
    "tid": 7,
    "created": "1730728052425",
    "is_seed": false,
    "lang": null,
    "pid": 7,
    "mod": 1,
    "agree_count": 6,
    "disagree_count": 2,
    "pass_count": 0,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Strawberry ice cream is my favorite; its perfect balance of sweet and tart brings summer to every scoop!",
    "tid": 2,
    "created": "1730728043381",
    "is_seed": false,
    "lang": null,
    "pid": 2,
    "mod": 1,
    "agree_count": 4,
    "disagree_count": 0,
    "pass_count": 4,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Mint chocolate chip is the worst flavor; it ruins ice cream with its toothpaste-like taste and confusing chocolate chunks!",
    "tid": 3,
    "created": "1730728045074",
    "is_seed": false,
    "lang": null,
    "pid": 3,
    "mod": 1,
    "agree_count": 2,
    "disagree_count": 6,
    "pass_count": 0,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Chocolate fudge brownie is my favorite ice cream flavor; its rich chocolate and chewy brownie bites create the perfect indulgence!",
    "tid": 1,
    "created": "1730728041973",
    "is_seed": false,
    "lang": null,
    "pid": 1,
    "mod": 1,
    "agree_count": 4,
    "disagree_count": 2,
    "pass_count": 2,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Mint chocolate chip is the perfect blend of refreshing mint and rich chocolate—an irresistible combo in every scoop!",
    "tid": 9,
    "created": "1730728056829",
    "is_seed": false,
    "lang": null,
    "pid": 9,
    "mod": 0,
    "agree_count": 6,
    "disagree_count": 2,
    "pass_count": 1,
    "count": 9,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Mint chocolate chip is my favorite ice cream flavor; the refreshing mint paired with chocolatey goodness is simply irresistible!",
    "tid": 5,
    "created": "1730728048538",
    "is_seed": false,
    "lang": null,
    "pid": 5,
    "mod": 0,
    "agree_count": 5,
    "disagree_count": 3,
    "pass_count": 1,
    "count": 9,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Chocolate fudge brownie is my favorite ice cream flavor; it perfectly combines rich chocolate and chewy brownie pieces!",
    "tid": 8,
    "created": "1730728054437",
    "is_seed": false,
    "lang": null,
    "pid": 8,
    "mod": 0,
    "agree_count": 3,
    "disagree_count": 2,
    "pass_count": 3,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Cream ice cream: it's even in the name!",
    "tid": 0,
    "created": "1730727998217",
    "is_seed": true,
    "lang": null,
    "pid": 0,
    "mod": 1,
    "agree_count": 7,
    "disagree_count": 0,
    "pass_count": 2,
    "count": 9,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Chocolate fudge brownie is my favorite ice cream flavor—indulgent, rich, and perfectly satisfying every time!",
    "tid": 6,
    "created": "1730728050318",
    "is_seed": false,
    "lang": null,
    "pid": 6,
    "mod": 0,
    "agree_count": 5,
    "disagree_count": 2,
    "pass_count": 1,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Mint chocolate chip is the perfect balance of refreshing mint and rich chocolate in every delicious bite!",
    "tid": 10,
    "created": "1730728058984",
    "is_seed": false,
    "lang": null,
    "pid": 10,
    "mod": 0,
    "agree_count": 5,
    "disagree_count": 2,
    "pass_count": 1,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  },
  {
    "txt": "Mint chocolate chip is my favorite ice cream flavor—refreshing, creamy, and the perfect blend of sweet and cool!",
    "tid": 4,
    "created": "1730728046871",
    "is_seed": false,
    "lang": null,
    "pid": 4,
    "mod": 1,
    "agree_count": 5,
    "disagree_count": 3,
    "pass_count": 0,
    "count": 8,
    "conversation_id": "6njerhau9r"
    ...
  }
]
```

The following JSON schema provides a description of the fields listed above
```
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "array",
  "items": {
    "type": "object",
    "properties": {
      "txt": {
        "type": "string",
        "description": "The text content of the comment"
      },
      "tid": {
        "type": "integer",
        "description": "The unique identifier for the comment",
        "example": 7
      },
      "created": {
        "type": "string",
        "description": "Timestamp of when the comment was created (epoch format)"
        "example": "1730728052425"
      },
      "is_seed": {
        "type": "boolean",
        "description": "Indicates whether this comment is a seed comment"
      },
      "lang": {
        "type": ["string", "null"],
        "description": "Language code of the comment, if available",
        "example": null
      },
      "pid": {
        "type": "integer",
        "description": "The unique identifier for the user that wrote the comment"
      },
      "mod": {
        "type": "integer",
        "description": "Moderator level or status associated with this response (0 - unmoderated; 1 - accepted)",
      },
      "agree_count": {
        "type": "integer",
        "description": "Number of participants who agree with this comment",
        "minimum": 0
      },
      "disagree_count": {
        "type": "integer",
        "description": "Number of participants who disagree with this comment",
        "minimum": 0
      },
      "pass_count": {
        "type": "integer",
        "description": "Number of participants who chose to pass on this comment",
        "minimum": 0
      },
      "count": {
        "type": "integer",
        "description": "Total number of interactions with this comment",
        "minimum": 0
      },
      "conversation_id": {
        "type": "string",
        "description": "Unique identifier for the survey this comment belongs to"
      }
    },
    "required": [
      "txt",
      "tid",
      "created",
      "is_seed",
      "lang",
      "pid",
      "mod",
      "agree_count",
      "disagree_count",
      "pass_count",
      "count",
      "conversation_id"
    ]
  }
}
```