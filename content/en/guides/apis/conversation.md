---
title: Conversations API
date: 2024-11-06
description: get all survey's comments and votes.
weight: 2
---

This API is used to get a survey's parameters and settings, such as the topic, the description and the participant count.

This page contains an example of a request and a response given from the API, with a schema describing the significant fields that can be found in the response.

## Example: Survey on ice cream

### Request example
```
GET http://<polisorbis-server-domain>/api/v3/conversations?conversation_id=<survey-id>
```

### Response example
This JSON is partial: an actual response contains other fields that are not specified here. The following are the more commonly used properties.

```
{
  "topic": "Ice cream",
  "description": "What is your favorite ice cream flavor?",
  "participant_count": 11,
  "auth_needed_to_vote": false,
  "auth_needed_to_write": false,
  "modified": "1730728069749",
  "created": "1730727970376",
  "auth_opt_openid": false,
  "ownername": "Anonymous",
  "is_owner": false,
  "conversation_id": "6njerhau9r"
}
```

The following JSON schema provides a description of the fields listed above
```
{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "type": "object",
  "properties": {
    "topic": {
      "type": "string",
      "description": "The topic of the survey"
    },
    "description": {
      "type": "string",
      "description": "A description of the survey"
    },
    "participant_count": {
      "type": "integer",
      "description": "The number of participants in the survey",
      "minimum": 0
    },
    "auth_needed_to_vote": {
      "type": "boolean",
      "description": "Whether authentication is required to vote",
      "example": false
    },
    "auth_needed_to_write": {
      "type": "boolean",
      "description": "Whether authentication is required to comment",
      "example": false
    },
    "modified": {
      "type": "string",
      "description": "Timestamp of the last modification (epoch format)",
      "example": "1730726628003"
    },
    "created": {
      "type": "string",
      "description": "Timestamp of the survey creation (epoch format)",
      "example": "1730726395388"
    },
    "auth_opt_openid": {
      "type": "boolean",
      "description": "Whether OpenID authentication is enabled",
      "example": false
    },
    "ownername": {
      "type": "string",
      "description": "Display name of the survey owner",
      "example": "Anonymous"
    },
    "is_owner": {
      "type": "boolean",
      "description": "Whether the current user is the owner of the survey",
      "example": false
    },
    "conversation_id": {
      "type": "string",
      "description": "Unique identifier for the survey conversation"
    }
  },
  "required": [
    "topic",
    "description",
    "participant_count",
    "auth_needed_to_vote",
    "auth_needed_to_write",
    "modified",
    "created",
    "auth_opt_openid",
    "ownername",
    "is_owner",
    "conversation_id"
  ]
}
```