---
title: Best Practices
description: Hints to run a pool with PolisOrbis
date: 2024-01-28
weight: 3
menu:
  main:
    weight: 3
---

{{% pageinfo color="info" %}}

This section covers:
- responsibility, capacity and cost planning to run a poll with PolisOrbis
- what deploy model to use or what existing services adopt
- hints to conduct a poll with PolisOrbis
{{% /pageinfo %}}


If you need to conduct a survey using PolisOrbis you have two alternatives:
- run your own instance of PolisOrbis Service in your infrastructure
- use an existing managed services asking permission to the service provider

Even if PolisOrbis code is free, to conduct an on-line opinions' survey always requires some resources:
- you must plan effort to setup, moderate, summarize and close the survey (depending from the survey characteristics)
- you must provide resources to publicize your survey
- if you opt to use a 3rd party service, you probably have to pay some service fees 
- if you opt to deploy your PolisOrbis service you must pay your infrastructure and service operation costs, by your personnel and/or external technical consultants

You must also take into account that running an on-line service always implies some responsibilities

## Using your own instance of PolisOrbis

Pros:
- attached to the identity service of your choice
- complete control on user access
- complete access to the internal DB
- great degree of UI customization
- optimized scalability support for big survey ( > 10000  users)
- you can expand the code with your own functionality for a better integration with your system
- no external services costs
- no license cost (Open source License)
  
Cons:
- you need your own hw & network infrastructure
- you must have good skills as a service provider and you must know cloud services, kubernetes, docker and Postgress
- no support on operation: you have to pay a consultant
- infrastructure requires a minimum capacity that can be oversized for just some small surveys ( < 1000 users): 
  there are some fixed infrastructure cost you have to pay even if no surveys are done.

## Using a 3rd party service
Pros:
- You do not have to manage technical infrastructure and concentrate only on survey contents
- Troubles in operations are managed by the service providers according defined SLA (service level agreement)
- cost are convenient for  short term, low number of participants surveys because the fixed cost are shared.
- high availability
  
Cons:
- you relay on the Identity server chosen by the service provider
- yoy have only a limited control on user access (depending from the service configuration) complete access to the internal DB
- no UI customization
- operation fee


### using PolisOrbis sandbox
The PolisOrbis sandbox service (https://demo.polisorbis.eu/) is free for all to test the latest version of the PolisOrbis service.
See [privacy policy](https://doc.polisorbis.copernicani.it/guides/sandbox/sandbox-privacy/) and [Term of Use](https://doc.polisorbis.copernicani.it/guides/sandbox/sandbox-tos/)


### using PolisOrbis Copernicani Instance
The [Copernicani PolisOrbis service](https://polisorbis.copernicani.it/) can be used by Orbis pilots by:
- Using the Orbis identity provider under the responsibility of the Orbis Consortium (as soon ass it will be available).
- Signing a contract containing SLA and a waiver, releasing Copernicani from liability for the content of your survey. [Example document link TBD]
- Contributing to the service operation cost (details in Basecamp)

  
## Responsibilities
Besides this running an on-line service requires some legal responsibilities:

- you must write and execute your policies to moderate/control user behavior inside your survey
- you must ensure that no sensitive data are shared inside the user generated comments

If you run your instance of PolisOrbis:
- you need to plan legal effort to write your Term of Services and Your Privacy policy

If you use a 3rd-party service:
- you will probably have to sign a waiver for the provider, releasing it from liability for the content of your survey.
- you will have to negotiate with the service provider a SLA and service costs
- you must agree with the service provider TOS and Privacy Policy



## other best practices
For a detailed methods paper, see [Polis: Scaling Deliberation by Mapping High Dimensional Opinion Spaces](https://www.e-revistes.uji.es/index.php/recerca/article/view/5516/6558).
