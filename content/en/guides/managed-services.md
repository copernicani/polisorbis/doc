---
title: Using a managed service
description: Using a ready-to-use PolisOrbis service managed by a service provider
date: 2024-03-13
weight: 2
---
If you do not want to create your instance of poll web application, you can consider to use one of the services provided by some associations.

The https://polisorbis.copernicani.it/ is the reference  deployment of the PolisOrbis codebase, it is  is provided by the Copernicani Association, it is scalable, GDPR compliant and requires a signed  agreement with the Copernicani Association before to be used.

Besides the PolisOrbis,  https://pol.is/ services that is the reference deployment for the original Polis project. This services is provided by The Computational Democracy Project and suitable for small polls and it is free upon registration but it is based in US and it is not GDPR compliant. 

See more info in the [Best Practice section]({{<ref "best-practices">}})