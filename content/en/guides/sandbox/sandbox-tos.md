---
title:  Term of use of PolisOrbis sandbox
date: 2024-03-15
---

The service available at <https://demo.polisorbis.eu> (the "sandbox") is managed by the [Copernicani Association](https://copernicani.it/) ( "Copernicani" ) on behalf of the [Orbis Project](https://www.polis-orbis.eu) ("Orbis").

The objective of the sandbox is to allow people to gain an idea of how the PolisOrbis project works in a simulated environment that is reset every week.

**YOU MUST NOT USE the sandbox for any real poll**.

All data, polls and registrations, in the sandbox is cleared at least every week, but more frequent clearing could happens.


## Eligibility
 You must be at least sixteen (16) years of age to use the Service.
 By agreeing to these Terms, you represent and warrant to us: (i) that you are at least sixteen (16) years of age; (ii) that you have not previously been suspended or removed from the Service; and (iii) that your registration and your use of the Service is in compliance with any and all applicable laws and regulations.
 If you are using the Service on behalf of an entity, organization, or company, you represent and warrant that you have the authority to bind such organization to these Terms and you agree to be bound by these Terms on behalf of such organization.

## Accounts and Registration
 To access some features of the Service, you must register for an account.
 When you register for an account, you may be required to provide us with some information about yourself as further described in our Privacy Policy.
 You are solely responsible for maintaining the confidentiality of your account and password.
 Copernicani will not be held liable for actions which occur under your account due to your credentials having been discovered due to no fault of PolisOrbis project.

 If you have reason to believe that your account is no longer secure, then you must immediately notify us at <privacy@copernicani.it>.


## User Content

### User Content Generally
Certain features of the Service may permit users to post content, including messages, questions, responses, data, text, and other types of works (collectively, "User Content") and to publish User Content on the Service.
You retain copyright and any other proprietary rights that you may hold in the User Content that you post to the Service, but grant the limited licenses below.

### Limited License Grant to the Copernicani Association
By posting or publishing User Content, you grant Orbis Project a worldwide, irrevocable, perpetual, non-exclusive, royalty-free, fully-paid-up right and license (with the right to sublicense) to host, store, transfer, display, perform, reproduce, modify, and distribute your User Content, in whole or in part, in any media formats and through any media channels (now known or hereafter developed).
Any such use of your User Content by Orbis project may be without any compensation paid to you.

### Limited License Grant to Other Users
By posting User Content or sharing User Content with another user of the Service, you hereby grant to other users of the Service a non-exclusive license to access and use such User Content as permitted by these Terms and the functionality of the Service.

### User Content Representations and Warranties
  You are solely responsible for your User Content and the consequences of posting or publishing User Content.
  By posting and publishing User Content, you affirm, represent, and warrant that:
    - you are the creator and owner of, or have the necessary licenses, rights, consents, and permissions to use and to authorize Orbis project and users of the Service to use and distribute your User Content as necessary to exercise the licenses granted by you in this Section and in the manner contemplated by the Copernicani Association and these Terms;
    - and your User Content, and the use thereof as contemplated herein, does not and will not: (i) infringe, violate, or misappropriate any third-party right, including any copyright, trademark, patent, trade secret, moral right, privacy right, right of publicity, or any other intellectual property or proprietary right;
    - or (ii) slander, defame, libel, or invade the right of privacy, publicity or other property rights of any other person.

### User Content Disclaimer
  We are under no obligation to edit or control User Content that you or other users post or publish, and will not be in any way responsible or liable for User Content.
  The Copernicani Association may, however, at any time and without prior notice, screen, remove, edit, or block any User Content that in our sole judgment violates these Terms or is otherwise objectionable.
  You understand that when using the Service you will be exposed to User Content from a variety of sources and acknowledge that User Content may be inaccurate, offensive, indecent or objectionable.
You agree to waive, and hereby do waive, any legal or equitable rights or remedies you have or may have against the Copernicani Association with respect to User Content.
We expressly disclaim any and all liability in connection with User Content.
If notified by a user or content owner that User Content allegedly does not conform to these Terms, we may investigate the allegation and determine in our sole discretion whether to remove the User Content, which we reserve the right to do at any time and without notice.
For clarity, Copernicani Association does not permit copyright-infringing activities on the Service.

### DMCA Takedown Requests
If you believe your copyright-protected work was posted to our Service without authorization, you may notify in accordance with the Digital Millenium Copyright Act at the following email address: <privacy@copernicani.it>.

## Prohibited Conduct
**BY USING THE SERVICE YOU AGREE NOT TO:**
  * use the service for any purpose different to test the PolisOrbis functionalities
  * use the Service for any illegal purpose, or in violation of any local, state, national, or international law;
  * violate, or encourage others to violate, the rights of third parties, including by infringing or misappropriating third party intellectual property rights;
  * post, upload, or distribute any User Content or other content that is unlawful, defamatory, libelous, inaccurate, or that a reasonable person could deem to be objectionable, profane, indecent, pornographic, harassing, threatening, embarrassing, hateful, or otherwise inappropriate;
  * interfere with security-related features of the Service, including without limitation by (i) disabling or circumventing features that prevent or limit use or copying of any content, or (ii) reverse engineering or otherwise attempting to discover the source code of the Service or any part thereof except to the extent that such activity is expressly permitted by applicable law; 
  * interfere with the operation of the Service or any user's enjoyment of the Service, including without limitation by (i) uploading or otherwise disseminating viruses, adware, spyware, worms, or other malicious code, (ii) making unsolicited offers or advertisements to other users of the Service, (iii) attempting to collect, personal information about users or third parties without their consent; or (iv) interfering with or disrupting any networks, equipment, or servers connected to or used to provide the Service, or violating the regulations, policies, or procedures of such networks, equipment, or servers;
  * perform any fraudulent activity including impersonating any person or entity, claiming false affiliations, accessing the Service accounts of others without permission, or falsifying your age or date of birth; 
  * sell or otherwise transfer the access granted herein or any Materials (as defined in Section below) or any right or ability to view, access, or use any Materials; or
  * attempt to do any of the foregoing in this Section , or assist or permit any persons in engaging in any of the activities described in this Section.

## Termination of Use; Discontinuation and Modification of the Service
 If you violate any provision of these Terms, your permission to use the Service will be terminated.
 Additionally, COpernicani Association, in its sole discretion may terminate your user account on the Service or suspend or terminate your access to the Service at any time, with or without notice.
 We also reserve the right to modify or discontinue the Service at any time (including, without limitation, by limiting or discontinuing certain features of the Service) without notice to you.
 We will have no liability whatsoever on account of any change to the Service or any suspension or termination of your access to or use of the Service.
 Your account is automatically terminated every service reset (one a week).


## Privacy Policy; Additional Terms
 Please read [Privacy Policy](({{< ref "sandbox-privacy" >}}))  carefully for information relating to our collection, use, storage and disclosure of your personal information. 
 The  Privacy Policy is hereby incorporated by reference into, and made a part of, these Terms. 

## Additional Terms
 Your use of the Service is subject to any and all additional terms, policies, rules, or guidelines applicable to the Service or certain features of the Service that we may post on or link to on the Service (the "Additional Terms"), such as end-user license agreements for any downloadable applications that we may offer, or rules applicable to particular features or content on the Service, subject to Section 9 below. 
 All such Additional Terms are hereby incorporated by reference into, and made a part of, these Terms. 
 
## Modification of these Terms
 We reserve the right, at our discretion, to change these Terms on a going-forward basis at any time. 
 Please check these Terms periodically for changes. 
 In the event that a change to these Terms materially modifies your rights or obligations, you will be required to accept such modified terms in order to continue to use the Service. 
 Material modifications are effective upon your acceptance of such modified Terms. 
 Immaterial modifications are effective upon publication. 
 For the avoidance of doubt, disputes arising under these Terms will be resolved in accordance with these Terms in effect that the time the dispute arose.


## Indemnity
 You agree that you will be responsible for your use of the Service, and you agree to defend, indemnify, and hold harmless The Copernicani Association and its officers, directors, employees, consultants, affiliates, subsidiaries and agents (collectively, the The Orbis Project) from and against any and all claims, liabilities, damages, losses, and expenses, including reasonable attorneys' fees and costs, arising out of or in any way connected with
 (i) your access to, use of, or alleged use of the Service;
 (ii) your violation of these Terms or any representation, warranty, or agreements referenced herein, or any applicable law or regulation;
 (iii) your violation of any third-party right, including without limitation any intellectual property right, publicity, confidentiality, property or privacy right; or
 (iv) any disputes or issues between you and any third party.
We reserve the right, at our own expense, to assume the exclusive defense and control of any matter otherwise subject to indemnification by you (and without limiting your indemnification obligations with respect to such matter), and in such case, you agree to cooperate with our defense of such claim.

## Disclaimers
 No Warranties THE SERVICE AND ALL MATERIALS AND CONTENT AVAILABLE THROUGH THE SERVICE ARE PROVIDED "AS IS" AND ON AN "AS AVAILABLE" BASIS, WITHOUT WARRANTY OR CONDITION OF ANY KIND, EITHER EXPRESS OR IMPLIED.
 THE MATH & DEMOCRACY ENTITIES SPECIFICALLY (BUT WITHOUT LIMITATION) DISCLAIM ALL WARRANTIES OF ANY KIND, WHETHER EXPRESS OR IMPLIED, RELATING TO THE SERVICE AND ALL MATERIALS AND CONTENT AVAILABLE THROUGH THE SERVICE, INCLUDING BUT NOT LIMITED TO (i) ANY IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, QUIET ENJOYMENT, OR NON-INFRINGEMENT; AND (ii) ANY WARRANTIES ARISING OUT OF COURSE OF DEALING, USAGE, OR TRADE.
 THE MATH & DEMOCRACY ENTITIES DO NOT WARRANT THAT THE SERVICE OR ANY PART THEREOF, OR ANY MATERIALS OR CONTENT OFFERED THROUGH THE SERVICE, WILL BE UNINTERRUPTED, SECURE, OR FREE OF ERRORS, VIRUSES, OR OTHER HARMFUL COMPONENTS, AND DO NOT WARRANT THAT ANY OF THE FOREGOING WILL BE CORRECTED.
 NO ADVICE OR INFORMATION, WHETHER ORAL OR WRITTEN, OBTAINED BY YOU FROM THE SERVICE OR ANY MATERIALS OR CONTENT AVAILABLE ON OR THROUGH THE SERVICE WILL CREATE ANY WARRANTY REGARDING ANY OF THE MATH & DEMOCRACY ENTITIES OR THE SERVICE THAT IS NOT EXPRESSLY STATED IN THESE TERMS.
 YOU ASSUME ALL RISK FOR ALL DAMAGES THAT MAY RESULT FROM YOUR USE OF OR ACCESS TO THE SERVICE, YOUR DEALINGS WITH OTHER SERVICE USERS, AND ANY MATERIALS OR CONTENT AVAILABLE THROUGH THE SERVICE.
 YOU UNDERSTAND AND AGREE THAT YOU USE THE SERVICE AND USE, ACCESS, DOWNLOAD, OR OTHERWISE OBTAIN MATERIALS OR CONTENT THROUGH THE SERVICE AND ANY ASSOCIATED SITES OR SERVICES AT YOUR OWN DISCRETION AND RISK, AND YOU WILL BE SOLELY RESPONSIBLE FOR ANY DAMAGE TO YOUR PROPERTY (INCLUDING YOUR COMPUTER SYSTEM USED IN CONNECTION WITH THE SERVICE) OR LOSS OF DATA THAT RESULTS FROM THE USE OF THE SERVICE OR THE DOWNLOAD OR USE OF SUCH MATERIALS OR CONTENT.
 SOME JURISDICTIONS MAY PROHIBIT A DISCLAIMER OF WARRANTIES AND YOU MAY HAVE OTHER RIGHTS THAT VARY FROM JURISDICTION TO JURISDICTION.

## Limitation of Liability
 IN NO EVENT WILL THE POLISORBIS AUTHORS, COPERNICANI AND ORBIS PROJECT BE LIABLE TO YOU FOR ANY INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES (INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, GOODWILL, USE, DATA, OR OTHER INTANGIBLE LOSSES) ARISING OUT OF OR RELATING TO YOUR ACCESS TO OR USE OF, OR YOUR INABILITY TO ACCESS OR USE, THE SERVICE OR ANY MATERIALS OR CONTENT ON THE SERVICE, WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE), STATUTE OR ANY OTHER LEGAL THEORY, WHETHER OR NOT THE MATH & DEMOCRACY ENTITIES HAVE BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE.
 SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR LIMITATION OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES.
 ACCORDINGLY, THE ABOVE LIMITATION MAY NOT APPLY TO YOU.
 EACH PROVISION OF THESE TERMS THAT PROVIDES FOR A LIMITATION OF LIABILITY, DISCLAIMER OF WARRANTIES, OR EXCLUSION OF DAMAGES IS TO ALLOCATE THE RISKS UNDER THESE TERMS BETWEEN THE PARTIES.
 THIS ALLOCATION IS AN ESSENTIAL ELEMENT OF THE BASIS OF THE BARGAIN BETWEEN THE PARTIES.
 EACH OF THESE PROVISIONS IS SEVERABLE AND INDEPENDENT OF ALL OTHER PROVISIONS OF THESE TERMS.
 THE LIMITATIONS IN THIS SECTION 13 WILL APPLY EVEN IF ANY LIMITED REMEDY FAILS OF ITS ESSENTIAL PURPOSE.

## Governing Law
 These Terms shall be governed by the laws of Italy in the EU without regard to conflict of law principles.
 To the extent that any lawsuit or court proceeding is permitted hereunder, you and Copernicani Association agree to submit to the personal and exclusive jurisdiction of the courts of Milan, ITALY for the purpose of litigating all such disputes.
 We operate the Service from our offices in Milan, and we make no representation that Materials included in the Service are appropriate or available for use in other locations.

## General
 These Terms, together with the Privacy Policy and any other agreements expressly incorporated by reference herein, constitute the entire and exclusive understanding and agreement between you and Copernicani Association regarding your use of and access to the Service, and except as expressly permitted above may be amended only by a written agreement signed by authorized representatives of all parties to these Terms.
 You may not assign or transfer these Terms or your rights hereunder, in whole or in part, by operation of law or otherwise, without our prior written consent.
 We may assign these Terms at any time without notice.
 The failure to require performance of any provision will not affect our right to require performance at any time thereafter, nor shall a waiver of any breach or default of these Terms or any provision of these Terms constitute a waiver of any subsequent breach or default or a waiver of the provision itself.
 Use of section headers in these Terms is for convenience only and shall not have any impact on the interpretation of particular provisions.
 In the event that any part of these Terms is held to be invalid or unenforceable, the unenforceable part shall be given effect to the greatest extent possible and the remaining parts will remain in full force and effect.
 Upon termination of these Terms, any provision that by its nature or express terms should survive will survive such termination or expiration, including, but not limited to, Sections 1, 3,.

## Dispute Resolution and Arbitration

### Generally
In the interest of resolving disputes between you and the Copernicani Association in the most expedient and cost effective manner, you and the Copernicani Association agree that any and all disputes arising in connection with these Terms shall be resolved by binding arbitration.
Arbitration is more informal than a lawsuit in court.
Arbitration uses a neutral arbitrator instead of a judge or jury, may allow for more limited discovery than in court, and can be subject to very limited review by courts.
Arbitrators can award the same damages and relief that a court can award.
Our agreement to arbitrate disputes includes, but is not limited to all claims arising out of or relating to any aspect of these Terms, whether based in contract, tort, statute, fraud, misrepresentation or any other legal theory, and regardless of whether the claims arise during or after the termination of these Terms.
YOU UNDERSTAND AND AGREE THAT, BY ENTERING INTO THESE TERMS, YOU AND COPERNICANI ASSOCIATION ARE EACH WAIVING THE RIGHT TO A TRIAL BY JURY OR TO PARTICIPATE IN A CLASS ACTION.

### Exceptions
Notwithstanding, we both agree that nothing herein will be deemed to waive, preclude, or otherwise limit either of our right to (i) bring an individual action in small claims court, (ii) pursue enforcement actions through applicable federal, state, or local agencies where such actions are available, (iii) seek injunctive relief in a court of law, or (iv) to file suit in a court of law to address intellectual property infringement claims.


## Consent to Electronic Communications
 By using the Service, you consent to receiving certain electronic communications from us as further described in the service Privacy Policy.
 Please read the service Privacy Policy to learn more about your choices regarding our electronic communications practices.
 You agree that any notices, agreements, disclosures, or other communications that we send to you electronically will satisfy any legal communication requirements, including that such communications be in writing. 

## Contact Information
 The services hereunder are offered by the Copernicani Association, located at PPiazza Repubblica, 1 - 20121 Milano - ITALY
 You may contact us by sending correspondence to the foregoing address using one of the meand described in the  the https://copernicani.it site.


