---
title: Try PolisOrbis
description: Play with PolisOrbis in a sandbox
date: 2024-03-13
weight: 1
---

{{% pageinfo color="info" %}}
The sandbox service is under construction and available as preview
{{% /pageinfo %}}

You can try PolisOrbis at https://demo.polisorbis.eu/ . This is a sandbox installation that is not suitable for production. 

The PolisOrbis sandbox has the following limitations:

- its database it is cleared every week
- it runs the latest code version (potentialli instable)
- e-mail dispaching and verification is disabled
- no Open Id SSO nor EUDI authentication is available

The sandbox service is installed and maintained using the [Deploy Blueprint](https://gitlab.com/copernicani/polisorbis/deploy-blueprints) *infrastructure as a code*, using the *aws-ecs2* manifest under the responsibility of [Copernicani association](https://copernicani.it).



