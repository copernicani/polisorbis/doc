---
title:  PolisOrbis sandbox privacy policy
date: 2024-03-15
---

This privacy policy (this "Policy") describes the collection of personal information by the Copernicani Association, a  nonprofit association ("the Copernicani Association," "we," or "us") from users of our Web site at "https://demo.polisorbis.eu/" (the "Site"), as well as all related applications, widgets, software, tools, and other services provided by us and on which a link to this Policy is displayed (collectively, together with the Site, our "Services").
This Policy also describes our use and disclosure of such information.
By using our Services, you consent to the collection, use, and disclosure of personal information in accordance with this Policy.
This Policy is incorporated by reference into the Terms of Use and subject to the provisions of the Terms of Use.
If you reside in the European Economic Area or Switzerland, please see International Visitors and Supplemental Notice to EU Data Subjects, below, which contains information about your rights and how to contact us.

## Personal Information

"Personal Information" as used in this Policy, is information that specifically identifies an individual, such as an individual's name, or e-mail address.

Personal Information does not include "aggregate" or other non-personally identifiable information.
Aggregate information is information that we collect about a group or category of products, services, or users that is not personally identifiable or from which individual identities are removed.
We may use and disclose aggregate information, and other non-personally identifiable information, for various purposes.

## Collection of Information

### Collection of Voluntarily-Provided Information

We collect Personal Information that our users provide to us in a variety of ways on our Services.
These include the following:

1. **User Accounts and Profiles.** &nbsp;
 Our Services may give you the ability to register for a Company account or to create and update a user profile on the applicable Service.
 If we offer user account or profile functionality on the Services, we will collect the Personal Information that you provide to us in the course of registering for an account or creating or updating a user profile.
 This information may include e-mail address, and other information about you.
 We may indicate that some personal information is required for you to register for the account or to create the profile, while some is optional.
 

1. **Interactive Features.** &nbsp;
 Our Services may contain interactive functionality that allows you to engage with other users on the Services, post questions to other users, answer questions of other users, post comments to questions and other forums, to upload other content (the 'User Materials'), participate in surveys, and otherwise to interact with the Services and with other users.
 If you use any interactive functionality on our Services that request or permit you to provide us with personal information (including, for example, any services that allow you to post User Materials on any of our Services), we collect the Personal Information that you provide to us in the course of using these interactive features.

1. **Correspondence.** &nbsp;
 If you contact us by e-mail, using a contact form on the Services, or by mail, fax, or other means, we collect the Personal Information contained within, and associated with, your correspondence.

### Automatically Collected Information

When you visit our Services, some information is collected automatically.
For example, when you access our Services, we may automatically collect your browser's Internet Protocol (IP) address, your browser type, the nature of the device from which you are visiting the Services (e.g., a personal computer or a mobile device), the identifier for any handheld or mobile device that you may be using, the Web site that you visited immediately prior to accessing any Web-based Services, the actions you take on our Services, and the content, features, and activities that you access and participate in on our Services.
We may collect this information automatically using technologies such as cookies.
We use automatically-collected information to administer, operate, and improve the Website and our other services and systems.
We may use and disclose information collected by automatic means in aggregate form or otherwise in a non-personally identifiable form.

### Information from Other Sources

We may receive information about you, including Personal Information, from third parties and third party application programming interfaces, as well as authentication service providers, and may combine this information with other Personal Information we maintain about you.
If we do so, this Policy governs any combined information that we maintain in personally identifiable format.

## Use of Personal Information

We use Personal Information to provide services and information that you request; to enhance, improve, operate, and maintain our Services, our programs, services, Web sites, and other systems; to prevent fraudulent use of our Services and other systems; to prevent or take action against activities that are, or may be, in violation of our Terms of Use or applicable law; to tailorcontent, and other aspects of your experience on and in connection with the Services; to maintain a record of our dealings with you; for other administrative purposes; and for any other purposes that we may disclose to you at the point at which we request your Personal Information, and pursuant to your consent.

We may also use your Personal Information:
* To process information you have directed us to process.
* To communicate with you (such as through email) about services you have subscribed for, changes to our policies (such as this Privacy Policy and our Terms of Service), or to provide important notices.
* To provide information or content that you’ve subscribed to receive.
* To set up and maintain your account, and to do all other things required for providing our services, such as backing up and restoring your data.
* To understand how our services are used, to monitor and prevent problems with our Site and/or services, and to improve our Site and/or services.
* To provide customer support.
* To update, modify, and analyze our records, and to identify prospective customers with their consent.
* To identify and analyze trends, track user activity on the Site, and understand what Site visitors are looking for with their consent where necessary.
* To create, monitor, and improve marketing campaigns with visitor consent.
* To inform you of new services, upcoming events, offers, and other information that we believe will be of interest to you, with your consent.
* To ask you to participate in a poll or survey, or to request feedback about our services, with your consent.
* For any other purpose that we describe to you when you provide information to us and for which you provide your consent.

## Disclosure of Personal Information

Except as described in this Policy, we will not disclose your Personal Information that we collect on the Services to third parties without your consent. We may disclose information to third parties if you consent to us doing so, as well as in the following circumstances:

### Service Providers

We may disclose Personal Information to third-party service providers (e.g., data storage and processing resources) that assist us in our work. We limit the Personal Information provided to these service providers to that which is reasonably necessary for them to perform their functions, and we require them to agree to maintain the confidentiality of such Personal Information.


### Technical Infrastructure
* We use AMAZON AWS’s cloud  services, including a hosted Postgres database, where we store all information. Information about how AWS handles: <https://aws.amazon.com/privacy/>

## To Protect our Interests
We also disclose Personal Information if we believe that doing so is legally required, or is in our interest to protect our property or other legal rights (including, but not limited to, enforcement of our agreements), or the rights or property of others, or otherwise to help protect the safety or security of our Services and other users of the Services.

## Choice

We may allow you to post User Materials in an anonymous or non-anonymous manner, which may affect which User Materials may be shared with third party advertisers.
Please review the relevant settings before posting User Materials.
Please note that some interactions on the Service are dedicated non-anonymous by the moderators of those interactions.
You will be notified that a given interaction is non-anonymous before posting any User Materials to such an interaction.

Additionally, we may allow you to view and modify settings relating to the nature and frequency of promotional communications that you receive from us.
Please be aware that if you opt-out of receiving commercial e-mail from us, it may take up to ten business days for us to process your opt-out request, and you may receive commercial e-mail from us during that period.
Additionally, even after you opt-out from receiving commercial messages from us, you will continue to receive administrative messages from us regarding our Services.

## Access

If we offer the ability to create temporary user accounts or profiles on our Services, you may have the ability to access and update many categories of personal information that you provide to us by logging in to your account and accessing your account settings.
Your data will be cleared at leas every week.


## Links

The Services may contain links to other Web sites, products, or services that we do not own or operate.
The Services may contain links to Third-Party Sites such as social networking services.
If you choose to visit or use any Third-Party Sites or products or services available on or through such Third-Party Sites, please be aware that this Policy will not apply to your activities or any information you disclose while using those Third-Party Sites or any products or services available on or through such Third-Party Sites.
We are not responsible for the privacy practices of these Third-Party Sites or any products or services on or through them.
Additionally, please be aware that the Services may contain links to Web sites and services that we operate but that are governed by different privacy policies.
We encourage you to carefully review the privacy policies applicable to any Web site or service you visit other than the Services before providing any Personal Information on them.


## Updates to this Policy

We may occasionally update this Policy.
When we do, we will also revise the 'last updated' date at the beginning of the Policy.
Your continued use of our Services after such changes will be subject to the then-current policy.
If we change this Policy in a manner that is materially less restrictive of our use or disclosure of your Personal Information, we will use reasonable efforts to notify you of the change and to obtain your consent prior to applying the change to any Personal Information that we collected from you prior to the date the change becomes effective.
We encourage you to periodically review this Policy to stay informed about how we collect, use, and disclose personal information.

## Contacting Us

If you have any questions or comments about this Policy, please contact us using the following contact information available on https://copernicani.it/ site 
 

