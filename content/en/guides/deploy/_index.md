---
title: Deploy
date: 2024-01-28
description: How to deploy the PolisOrbis application in your infrastructure
weight: 2
---

PolisOrbis is a [cloud-native application](https://www.redhat.com/en/topics/cloud-native-apps#overview) that consists of a set of containers built with Docker and some backing services running by a container orchestrator in a cloud infrastructure.

The type of infrastructure and orchestration depends mainly on the audience size for the polls you want to conduct. The PolisOrbis project natively supports three different types of deployment:
- A *scalable production deployment*, suitable for large polls involving many thousands of users (potentially millions).
- A *minimal production deployment*, suitable for test and small polls involving up to less than 200 users and where security and availability is not critical.
- A *local deployment* aimed at code developers.

{{% pageinfo color="info" %}}
<i class="fa-solid fa-hand-point-right fa-2xl"></i> Publishing any web application online, such as PolisOrbis, entails costs and responsibilities. 

The [Best Practices](/guides/best-practices/) section of this manual exemplifies some typical cases and helps plan the costs, activities, and skills required to conduct a survey with PolisOrbis.
{{% /pageinfo %}}

For the first two types of deployment, the code (Infrastructure as Code) is provided directly using [Terraform](https://www.terraform.io/) and ready to run on the [AWS Cloud](https://aws.amazon.com/). The deploy blueprints are contained in the [deployment repository][]

The third type of deployment is described in the readme of the [PolisOrbis codebase](https://gitlab.com/copernicani/polisorbis/codebase).

If you want to deploy PolisOrbis in different infrastructure you can refer to the code in the [deployment repository][] as an example.

All deploy blueprints is released with a Free Open Source License

[deployment repository]: https://gitlab.com/copernicani/polisorbis/deploy-blueprints