---
title: "Best practices"
description: best practice for running conversations, and checklists.
type: "docs"
weight: 1

---
{{% pageinfo color="info" %}}
For a detailed methods paper, see [Polis: Scaling Deliberation by Mapping High Dimensional Opinion Spaces](https://www.e-revistes.uji.es/index.php/recerca/article/view/5516/6558).
{{% /pageinfo %}}

This section is under construction, in the meantime have a look to the[ original Polis documentation](https://compdemocracy.org/Usage/)