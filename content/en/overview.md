---
title: "PolisOrbis overview"
linkTitle: "Overview"
menu:
  main:
    weight: 1
type: "docs"
---
![Polisorbis](/images/polisorbis-logo.png)

**PolisOrbis** is an Open Source Approach to Survey Research: it is a real-time system designed for the efficient gathering, analysis, and understanding of the opinions expressed by large groups of people. It is one of the tools provided by the [EU Orbis Project](https://orbis-project.eu/)


## A platform for conversations
PolisOrbis serves as a unique platform for facilitating conversations. Participants contribute concise text statements or comments (limited to <140 characters), which are then distributed semi-randomly to other participants for voting. Votes can be cast by clicking on agree, disagree, or pass. Notably, PolisOrbis empowers conversation owners to create discussions that seamlessly engage up to hundreds of thousands and potentially millions of participants.

## Origins and Contributors
PolisOrbis is based on a fork of the [Polis project](https://github.com/compdemocracy/polis), by Computational Democracy Project. The PolisOrbis project is actively built and maintained by the [Copernicani association](https://copernicani.it) with the support of the [g0v.it community](https://g0v.it/). 

The PolisOrbis is open source software released under the AGPL3 license. All PolisOrbis code and documentation is available at https://gitlab.com/copernicani/polisorbis .

## Why PolisOrbis
The objectives of PolisOrbis is to improve the Polis project in order to:

- allowing safe while-label deployment and use of PolisOrbis services in Europe;
- ensure GDPR compliance;
- reducing (or making optional) any dependency on OTT services (Meta, X, etc) avoiding any user profiling by external entities;
- implement Open ID Connect authentication;
- experiment secure and anonymous conversation leveraging EUDI wallet and SSI technologies (if available from EU);
- improve deployment documentation and provide Infrastructure-as-Code resources to help any organization to setup and manage its owned service based on PolisOrbis codebase

In its highest ambition, PolisOrbis is a platform for enabling collective intelligence in human societies and fostering mutual understanding at scale in the tradition of nonviolent communication. 


## Reference deployments
PolisOrbis is a free  Open Source code, to be used it must be deployed in a network infrastructure of your choice. Here are some known existing on-line services:
- The Copernicani association maintains a full-featured scalable instance of PolisOrbis at [https://polisorbis.copernicani.it/](https://polisorbis.copernicani.it/).
- A sandbox service of PolisOrbis is freely available on [https://demo.polisorbis.eu/](https://demo.polisorbis.eu/). The database is cleared every week, and there is no SLA. Not to be used for production.
- The original [Pol.is](https://pol.is/) site can also be used for any non-commercial purpose that does not require GDPR compliance or a strong security.

{{% pageinfo color="info" %}}
<i class="fa-solid fa-hand-point-right fa-2xl"></i> have a look to the [Getting Started guides]({{< ref "guides" >}}) to put PolisOrbis in action in your own infrastructure
{{% /pageinfo %}}



PolisOrbis is part of the [Orbis project](https://orbis-project.eu) and has received funding from the European Union’s Horizon Europe Framework Programme under grant agreement No 101094765.
