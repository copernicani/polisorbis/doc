# ![PolisOrbis](static/images/polisorbis-logo.png)

# Documentation Portal
This repository contains the contents and the publishing procedures for the PolisOrbis application. 

This project uses the [Docsy][] as a [Hugo theme module][] and it is published by Copernicani in Netlify infrastructure at the URL [doc.polisorbis.copernicani.it][]

## Quickstart

run `docker-compose up --build`,  open your web browser and type `http://localhost:1313` in your navigation bar

## ©️  License

[AGPLv3 with additional permission under section 7](/LICENSE)


PolisOrbis is part of the Orbis project and has received funding from the European Union’s Horizon Europe Framework Programme under grant agreement No 101094765.


[Docsy]: https://github.com/google/docsy
[Hugo theme module]: https://gohugo.io/hugo-modules/use-modules/#use-a-module-for-a-theme
[doc.polisorbis.copernicani.it]: https://doc.polisorbis.copernicani.it/