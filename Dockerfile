FROM klakegg/hugo:ext-ubuntu-onbuild AS hugo

FROM nginx
COPY --from=hugo /target /usr/share/nginx/html
